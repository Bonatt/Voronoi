### Voronoi Diagrams

I learned about Voronoi diagrams from somewhere and decided to explore the implementation myself.
From [Wikipedia](https://en.wikipedia.org/wiki/Voronoi_diagram), 
>>>
In mathematics, a Voronoi diagram is a partitioning of a plane into regions based 
on distance to points in a specific subset of the plane. That set of points (called seeds, sites, 
or generators) is specified beforehand, and for each seed there is a corresponding region consisting 
of all points closer to that seed than to any other. These regions are called Voronoi cells"
>>>

I originally created a program ([VoronoiDiagram.py, e.g.](https://gitlab.com/Bonatt/Voronoi/blob/master/Single/Old/)) 
that generated a list of (random) _n_ non-zero numbers ("IDs") within some given range,
and _n_ random _x_, _y_ positions ("seed" sites) within a _w_ x _h_ grid of zeros.
Each ID was placed at a single site as shown below:
```
0 0 0 0 4 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 2
0 3 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 1 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
```
This is 1,2,3,4 seeded in to a 10x10 grid. 
Via loop over the grid, the value at each index was changed from 0 to the closest seed ID. 
The distance metric was Euclidean. The filled graph is shown below:
```
3 3 4 4 4 4 4 2 2 2
3 3 3 4 4 4 4 2 2 2
3 3 3 3 4 4 4 2 2 2
3 3 3 3 3 4 2 2 2 2
3 3 3 3 3 3 2 2 2 2
1 1 1 1 1 1 2 2 2 2
1 1 1 1 1 1 2 2 2 2
1 1 1 1 1 1 1 2 2 2
1 1 1 1 1 1 1 1 2 2
1 1 1 1 1 1 1 1 2 2
```

A few files later and I had fancier inputs and outputs but it was still all terminal-based.
I eventually expanded functionality...

### Single: generates and plots a single Voronoi diagram.

Here I created two programs: one to generate (Make) a Voronoi diagram, and one to plot (Draw) the diagram. 
The Make program generated a seeded grid and a Voronoi-filled grid using a method very similar to the one discussed above.
These two grid were saved to file
The Draw program loads the grids from file and generates a histogram. 
I wrestled with how to visualize the diagram. I originally attempted some kind of contour plot 
but eventually settled on a 2D histogram. 
By setting the z-axis appropriately, each unique ID could be given a unique color automatically by the histogram function. 
Caveat: because of the scale, some colors may appear to be very similar. The original, seeded positions are displayed as text.

![Single](/Single/hVoronoi.png)

I also gathered some data from the histogram. 
I could find the "center of mass" or "center of population" of each unique color.
<!--With the assumtion that the center of each region was its seed postion,
and that the center of all uniquely-colored regions was the average of the center of those regions,
the center of population for each color was found.-->
The center of population is displayed at a 45 degree angle. 
Because each ID can be chosen randomly, 
the center of population may be the center of two or more regions,
shifting the displayed text outside of that region.

And I could find the total area of each unique color:
```
Identifier: 1    Area: 2576, 2%
Identifier: 2    Area: 901, 1%
Identifier: 3    Area: 969, 1%
Identifier: 4    Area: 2145, 1%
Identifier: 6    Area: 626, 0%
...
```
I thought about how I could find the nodes and interfaces between each region but shelved the project before such implementation. 
But not before I attempted generating and plotting multiple Voronoi diagrams simultaneously...

### Multi: generates and plots a grid of Voronoi diagrams given some parameters.

I was tired of creating a new plot for every parameter change I had in mind: 
1. Size and shape of grid
2. Number of seeds (skipping trivial cases _n_ = 0, 1)
3. Number of unique seeds
4. How to vary 2 and 3 for each diagram
5. Number of diagrams

A file containing all relevant paramters was created.
I mainly wanted to see how the visualization changed as I increased the number of seeds. 
This number increased linearly, exponentially, or with perfect squares. 
The IDs were generated to be unique, random, or with some modulo.

These parameters are imported by a Make program and the generated grids are saved to file.

The Draw program imports the grid files and generates a histogram for each one.
Regardless of the number of grids imported, the divided plot will be square 
(I'm unreasonably proud of manually coding this given its triviality). 

Now the best part (plots that were created before git init). For each plot below, each grid increases by a perfect square.

__Maximum of 2 Unique, Randomly-chosen IDs__
![hVoronoiMulti_M=16_F=Random_R=2_N(M)=Square.png](/Multi/hVoronoiMulti_M=16_F=Random_R=2_N(M)=Square.png)
__Maximum of 3 Unique, Randomly-chosen IDs__ (Random choice from ID = [1,2,3])
![hVoronoiMulti_M=16_F=Random_R=3_N(M)=Square.png](/Multi/hVoronoiMulti_M=16_F=Random_R=3_N(M)=Square.png)
__Maximum of 3 Unique, Mod3-chosen IDs__ (ID = [1,2,3] repeating)
![hVoronoiMulti_M=16_F=Modulo_R=3_N(M)=Square.png](/Multi/hVoronoiMulti_M=16_F=Modulo_R=3_N(M)=Square.png)
__Maximum of 9 Unique, Randomly-chosen IDs__
![hVoronoiMulti_M=16_F=Random_R=9_N(M)=Square.png](/Multi/hVoronoiMulti_M=16_F=Random_R=9_N(M)=Square.png)
__Maximum of 289 Unique, Randomly-chosen IDs__
![hVoronoiMulti_M=16_F=Unique_R=289_N(M)=Square.png](/Multi/hVoronoiMulti_M=16_F=Unique_R=289_N(M)=Square.png)

Again, because of the poor resolution of the histogram scales, many of these plots look indistinguishable. 
For low unique IDs I was inpired by camouflage (the pattern, not the colorway).
