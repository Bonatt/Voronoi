import ROOT
import numpy as np
import os
import glob

from VoronoiParams import *




# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()...
def sqrt(x):
  return np.sqrt(x)




### See http://pcg.wikidot.com/pcg-algorithm:voronoi-diagram



### Before proceeding, delete all .txt files in Data
# From https://stackoverflow.com/questions/6996603/how-to-delete-a-file-or-folder
for f in glob.glob('Data/'+NofMStr+'/*.txt'):
  os.remove(f)

print ''
print M, 'matrices via', NofMStr,'function.'

### Loop through N salted events, skipping trivial cases n=0,1
for en,n in enumerate(NofM):


  # Static incremental list. All values unique.
  #ID = [i for i in range(1,n+1)] #== [i+1 for i in range(n)]
  # Repeat pattern to make more than one population of 1's, e.g., a la camo. Only max 9 unique values allowed. ID = 1-9
  #ID = [i%9+1 for i in range(n)]
  # Same as above, but randomized to for more fair number of 1's, e.g.
  #ID = [int(i) for i in np.random.uniform(1, 10, n)]
  ID = F(n)

  #print ''
  #print N, n, ID
  print 'Generating matrix '+str(en+1)+', '+str(minN)+'-'+str(n)+': '+str(ID)
  #print ''


  # If any ID would be greater than one digit, make all emptychars two+ digits to align better.
  emptychar = str('').zfill(len(str(max(ID))))
  # From https://docs.python.org/2/faq/programming.html#how-do-i-create-a-multidimensional-list . This is best/recommended?
  Grid = [[emptychar] * width for i in range(height)]


  ### Generate random x,y position. Does not check of two points overlap.
  xpos = [int(i) for i in np.random.uniform(0, width, n)]
  ypos = [int(i) for i in np.random.uniform(0, height, n)]


  ### Change the above n random positions into n ID
  for xp,yp,i in zip(xpos,ypos,ID):
    Grid[yp][xp] = i




  ### Print Grid neatly, with n random dots
  strGrid0 = [' '.join([str(x).zfill(len(str(max(ID)))) for x in Grid[y]]) for y in range(height)]

  with open('Data/'+NofMStr+'/Voronoi0_'+str(n).zfill(4)+'.txt', 'w') as file:

    for h in range(height):
      #print strGrid0[h]

      file.write(strGrid0[h]+'\n')

  #print ''




  ### Now do Voronoi
  for y0 in range(height):
    for x0 in range(width):
      # [0,0], [0,1], ...
      # [1,0], [1,1], ...

      rArray = []
      for x,y in zip(xpos,ypos):
        r = sqrt( (x-x0)**2 + (y-y0)**2 )
        rArray.append(r)

      Grid[y0][x0] = ID[rArray.index(min(rArray))] # Works for any ID list, not just range list




  ### Print Grid neatly, filled. And save to file.
  strGrid1 = [' '.join([str(x).zfill(len(str(max(ID)))) for x in Grid[y]]) for y in range(height)]

  with open('Data/'+NofMStr+'/Voronoi_'+str(n).zfill(4)+'.txt', 'w') as file:

    for h in range(height):
      #print strGrid1[h]
    
      file.write(strGrid1[h]+'\n')

  #print ''
