import ROOT
import numpy as np
import os
import glob
import math

from VoronoiMakeMulti import *
#from VoronoiParams import *






#///// Set global style /////
ROOT.gROOT.SetStyle('Plain')
ROOT.gStyle.SetPalette(55) # 51:blue. 52:lo=k/hi=w. 53:lo=r/hi=y, 56 rev. 54:lo=b/hi=y. 55:lo=b/hi=r, pale rainbow. none:rainbow 
# Palettes, see https://root.cern.ch/doc/master/classTColor.html#C05. Or for ROOT 5.34: https://people.nscl.msu.edu/~noji/colormap
#// Boxes
ROOT.gStyle.SetLegendFont(132)
ROOT.gStyle.SetStatFont(132)
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h')
ROOT.gStyle.SetTitleSize(0.05, 'h') #0.06
ROOT.gStyle.SetTitleBorderSize(0)
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz')
ROOT.gStyle.SetTitleSize(0.04, 'xyz') #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x') #1.15
ROOT.gStyle.SetTitleOffset(1, 'y') #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz')
ROOT.gStyle.SetLabelSize(0.04, 'xyz')
#// Text options
ROOT.gStyle.SetTitleFont(132, 't')
ROOT.gStyle.SetTextFont(132)
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g")
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0)
ROOT.gStyle.SetFrameBorderSize(0)
ROOT.gStyle.SetLegendBorderSize(1) #0,1
ROOT.gStyle.SetStatBorderSize(0)
ROOT.gStyle.SetTitleBorderSize(0)
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();




# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()...
def sqrt(x):
  return np.sqrt(x)




#VoronoiDiagrams = []
#VoronoiDiagrams0 = []

Files = filter(os.path.isfile, glob.glob('Data/'+NofMStr+'/Voronoi_*.txt'))
Files.sort()
Files0 = filter(os.path.isfile, glob.glob('Data/'+NofMStr+'/Voronoi0_*.txt'))
Files0.sort()
#nFiles = len(filter(os.path.isfile, glob.glob('Data/*.txt')))/2 # Divide by two because one is V0



print ''
### Initialize canvas and divide it, scaled by number of N
c = ROOT.TCanvas('c', 'c', 720*width/height, 720)
c.SetTopMargin(0)
c.SetBottomMargin(0)
c.SetLeftMargin(0)
c.SetRightMargin(0)
#c.Divide(int(round(sqrt(N))),int(round(sqrt(N))))
c.Divide(int(math.ceil(sqrt(M))), int(sqrt(M)))




hVoronoi0 = []
hVoronoi = []

for i,(f0,f) in enumerate(zip(Files0,Files)):
  print i,f0,f
  #if 'Voronoi0' in f: 
    #VoronoiDiagrams0.append(np.genfromtxt(f))
  V0 = np.genfromtxt(f0)
  #else: 
    #VoronoiDiagrams.append(np.genfromtxt(f))
  V = np.genfromtxt(f)

  hV0 = ROOT.TH2D(f, f, width,0,width, height,0,height)
  hV = ROOT.TH2D(f, f, width,0,width, height,0,height)
  hV.SetStats(0)
  #hV.SetTitle('N = '+str(i)+';;')
  hV.SetTitle('N = '+str(NofM[i])+';;') #str(f.split('/')[2].split('_')[1].split('.')[0].split('0')[-1])+';;')
  #hV.SetTitle(';;')


  for w in range(width):
    for h in range(height):
      hV0.Fill(w, h, V0[h][w])
      hV.Fill(w, h, V[h][w]) 

  hVoronoi0.append(hV0)
  hVoronoi.append(hV)

  c.cd(i+1)
  ROOT.gPad.SetTopMargin(0)
  ROOT.gPad.SetBottomMargin(0)
  ROOT.gPad.SetLeftMargin(0)
  ROOT.gPad.SetRightMargin(0)

  if i == 0:
    hVoronoi[i].Draw('a col')
  else:
    hVoronoi[i].Draw('a col same')
    #hVoronoi0[i].Draw('text same')





print ''
#c.Divide(int(math.ceil(sqrt(N))), int(sqrt(N)))
#c.SaveAs('hVoronoiMulti_'+str(int(math.ceil(sqrt(N))))+'x'+str(int(sqrt(N)))+'_N='+str(N)+'_'+NofMStr+'='+str(min(NofM))+'-'+str(max(NofM))+'.png')
#c.SaveAs('hVoronoiMulti_N='+str(N)+'_'+FStr+'_'+NofMStr+'='+str(min(NofM))+'-'+str(max(NofM))+'.png')
#c.SaveAs('hVoronoiMulti_N='+str(N)+'_'+FStr+'_'+NofMStr+'='+str(min(NofM))+'-'+str(max(NofM))+'.png')
#c.SaveAs('hVoronoiMulti_M='+str(M)+'_N(M)='+FStr(x)+'_N='+NofMStr+'='+str(min(NofM))+'-'+str(max(NofM))+'.png')
#c.SaveAs('hVoronoiMulti_M='+str(M)+'_N='+NofMStr+'='+str(min(NofM))+'-'+str(max(NofM))+'.png')
#c.SaveAs('hVoronoiMulti_M='+str(M)+'_F='+FStr+'_N(M)='+NofMStr+'='+'-'.join([str(fi) for fi in NofM])+'.png')
c.SaveAs('hVoronoiMulti_M='+str(M)+'_F='+FStr+'_R='+str(len(set(ID)))+'_N(M)='+NofMStr+'.png')
print ''

# R = range. Number of unique values
