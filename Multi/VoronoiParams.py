import numpy as np




### Create empty grid (array)
size = 30 # 30 shows the ID array, salted array, and filled array neatly. 68 high height, perfect width. 62 shows full filled only.
ratio = 1366./720.
height = size
width = int(round(size*ratio)) # If = size, grid is square.

### M matrices/grids.
M = 16

### To loop through N salted events, skipping trivial cases n=0,1
minN = 2




###### Loop function. N salted events is proportional to G grids. Increases as some function of M

# Linear
#for n in range(minN,G+minN):
#NofM = range(minN,M+minN); NofMStr = 'Linear'

# Square
#for n in [i*i for i in range(minN,G+minN)]:
NofM = [i*i for i in range(minN,M+minN)]; NofMStr = 'Square'

# Exp
#for n in [int(i*np.exp(i/5.)) for i in range(minN,M+minN)]:
N0 = 1.
Lambda = 0.5
#NofM = [int(N0*np.exp(i*Lambda)) for i in range(minN,M+minN)]; NofMStr = 'Exponential'



###### Vary diversity when randomized
minx = 1
maxx = 3 #10, 4

###### Unique or random values of x?
def F(x):

  # 1. Static incremental list. All values unique.
  #return [i for i in range(1,x+1)]
  
  # 2. Repeat pattern to make more than one population of 1's, e.g., a la camo. Only max 9 unique values allowed. ID = 1-9
  #return [i%(maxx-1)+1 for i in range(x)]

  # 3. Same as above, but randomized to for more fair number of 1's, e.g.
  return [int(i) for i in np.random.uniform(minx, maxx, x)]


### If 1 above, Unique. If 2, Mod. If 3, Random
#FStr = 'Unique'
#FStr = 'Modulo'
FStr = 'Random' #='+str(minx)+'-'+str(maxx)


#len(set(new_words))




'''
def FStr(x):
  # Linear
  #return 'Linear='+str(min(F(x)))+'-'+str(max(F(x))) 
  #str(min([i for i in range(1,x+1)]))+'-'+str(max([i for i in range(1,x+1)]))
  # Random
  return 'Random=1-9'
'''
