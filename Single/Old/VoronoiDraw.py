import ROOT
import numpy as np
#import math as math
#import os
#import linecache
#import sys
#import time
from VoronoiMake2 import *




#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(55); # 51:blue. 52:lo=k/hi=w. 53:lo=r/hi=y, 56 rev. 54:lo=b/hi=y. 55:lo=b/hi=r, pale rainbow. none:rainbow 
# Palettes, see https://root.cern.ch/doc/master/classTColor.html#C05. Or for ROOT 5.34: https://people.nscl.msu.edu/~noji/colormap
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(1);  #0,1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();




# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()...
def sqrt(x):
  return np.sqrt(x)





### See http://pcg.wikidot.com/pcg-algorithm:voronoi-diagram

### May get data via "VoronoiDiagram = np.genfromtxt('Voronoi.txt')", or just use what was created.
VoronoiDiagram = np.genfromtxt('Voronoi.txt') # Grid[0]
VoronoiDiagram0 = np.genfromtxt('Voronoi0.txt') #Grid0[:]


### The below was copied from proc/Maps/Medium
### Note that the printed matrix and the plotted matrix will differ by horz mirror. (0,0) is topleft for print, bottomleft for plots.
### Create canvas with no margins or titles
#c = ROOT.TCanvas('c', 'c', 720*width/height, 720)
#c.SetTopMargin(0)
#c.SetBottomMargin(0)
#c.SetLeftMargin(0)
#c.SetRightMargin(0)

### Initialize 2D graph to be filled
g2D = ROOT.TGraph2D()
g2D.SetTitle(';;')

### Initialize 2D hist to be filled
h2D = ROOT.TH2D('h2D', 'h2D', width,0,width, height,0,height)
h2D.SetStats(0)
h2D.SetTitle(';;')

### Initialize 2D hist to be filled with Grid0 points
h2D0 = ROOT.TH2D('h2D0', 'h2D0', width,0,width, height,0,height)
h2D0.SetStats(0)
h2D0.SetTitle(';;')
h2D0.SetLineColor(ROOT.kBlack)


### Create palette. One color per number.
# From https://root.cern.ch/doc/master/classTColor.html#C05
# From https://root-forum.cern.ch/t/th2-drawn-with-colz-option-colors-and-range/14225/8
#colors = ID[:]
#ROOT.gStyle.SetPalette(len(colors), colors)
#levels = ID[:]
levels = [i+1 for i in range(max(ID))]
h2D.SetContour(len(levels), np.double(levels))



### Go from y lines of x z values and put them into proper x,y,z to be graphed.
# Save x,y,z in case we need them.
#x = []
#y = []
#z = []
i = 0
for w in range(width):
  for h in range(height):
    #x.append(w)
    #y.append(h)
    #z.append(VoronoiDiagram[h][w])

    g2D.SetPoint(i, w, h, VoronoiDiagram[h][w])
    i = i + 1

    h2D.Fill(w,h,VoronoiDiagram[h][w])
    h2D0.Fill(w,h,VoronoiDiagram0[h][w]) #*1000

#g2D.Draw('colz')
#g2D.Draw('cont4z') # cont1 for lines; cont5 for more lines; cont2 for diff style lines; cont3 for b/w lines; cont4 for triangle colors




### Create canvas with no margins or titles for Histogram
c2 = ROOT.TCanvas('c2', 'c2', int(720*float(width)/height), 720)
c2.SetTopMargin(0)
c2.SetBottomMargin(0)
c2.SetLeftMargin(0)
c2.SetRightMargin(0)

h2D.Draw('a col')
h2D0.Draw('text same')


### Create canvas with no margins or titles for Histogram 2
'''
c3 = ROOT.TCanvas('c3', 'c3', int(720*float(width)/height), 720)
c3.SetTopMargin(0)
c3.SetBottomMargin(0)
c3.SetLeftMargin(0)
c3.SetRightMargin(0)

h2D.Draw('a cont3')
h2D0.Draw('text same')
'''





#c.SaveAs('gVoronoi.png')
c2.SaveAs('hVoronoi.png')
#c3.SaveAs('hVoronoi2.png')











### How to draw own lines between regions?
