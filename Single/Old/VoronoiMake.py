import ROOT
import numpy as np
#import math as math
#import os
#import linecache
#import sys
#import time



#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
#ROOT.gStyle.SetPalette(53);
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(1);  #0,1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();




# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()...
def sqrt(x):
  return np.sqrt(x)





### See http://pcg.wikidot.com/pcg-algorithm:voronoi-diagram



### Create empty grid (array)
size = 29 # 29 shows the ID array, salted array, and filled array neatly. 68 high height, perfect width. 62 shows full filled only.
width = size
height = size


### Let salted values be identified by integers (instead of colors, eg)
# n random dots on grid.
n = 100

# Static incremental list. All values unique.
ID = [i for i in range(1,n+1)]
# Repeat pattern to make more than one population of 1's, e.g., a la camo. Only max 9 unique values allowed. ID = 1-9
#ID = [i%9+1 for i in range(n)]
# Same as above, but randomized to for more fair number of 1's, e.g.
#ID = [int(i) for i in np.random.uniform(1, 10, n)]
print ''
print ID


# If any ID would be greater than one digit, make all emptychars two+ digits to align better.
emptychar = str('').zfill(len(str(max(ID))))
# From https://docs.python.org/2/faq/programming.html#how-do-i-create-a-multidimensional-list . This is best/recommended?
Grid0 = [[emptychar] * width for i in range(height)]
Grid = Grid[:]


### Generate random x,y position. Does not check of two points overlap.
xpos = [int(i) for i in np.random.uniform(0, width, n)]
ypos = [int(i) for i in np.random.uniform(0, height, n)]


### Change the above n random positions into n ID
for xp,yp,i in zip(xpos,ypos,ID):
  Grid[yp][xp] = i




### Print Grid neatly, with n random dots
print ''
#strGrid0 = [' '.join([str(x) for x in Grid[y]]) for y in range(height)]
strGrid0 = [' '.join([str(x).zfill(len(str(max(ID)))) for x in Grid[y]]) for y in range(height)]
for h in range(height):
  print strGrid0[h]
print ''




### Now do Voronoi
for y0 in range(height):
  for x0 in range(width):
    # [0,0], [0,1], ...
    # [1,0], [1,1], ...

    rArray = []
    for x,y in zip(xpos,ypos):
      r = sqrt( (x-x0)**2 + (y-y0)**2 )
      rArray.append(r)

    #Grid[y0][x0] = rArray.index(min(rArray))+1 # This only works when ID list is 1,2,3,4... nonrepeating and nonrandom
    Grid[y0][x0] = ID[rArray.index(min(rArray))] # Works for any ID list




### Print Grid neatly, filled. And save to file.
#strGrid1 = [' '.join([str(x) for x in Grid[y]]) for y in range(height)]
strGrid1 = [' '.join([str(x).zfill(len(str(max(ID)))) for x in Grid[y]]) for y in range(height)]
#for h in range(height):
#  print strGrid1[h]
#print ''

with open('Voronoi.txt', 'w') as file:

  for h in range(height):
    print strGrid1[h]
    
    file.write(strGrid1[h]+'\n')

print ''
