import ROOT
import numpy as np
import math as math
import os
import linecache
import sys
#import time



#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
#ROOT.gStyle.SetPalette(53);
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(1);  #0,1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();




# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()...
def sqrt(x):
  return np.sqrt(x)





### See http://pcg.wikidot.com/pcg-algorithm:voronoi-diagram



### Create empty grid (array)
l = 10
# Make is square
width = l
height = l
### Method 1
#Grid = np.empty([height,width])
#Grid.fill(0)
### Unfinished Method
# ["" for x in range(l)]
### Method 2
#Grid = np.zeros([height,width])
### Method 3
# From https://docs.python.org/2/faq/programming.html#how-do-i-create-a-multidimensional-list
emptychar = 0#' ' #0
Grid = [[emptychar] * width for i in range(height)]


### Let serial numbers be labelled as such. Indentified by integers (instead of numbers, eg)
#codes = ['1', '2', '3', '4']
# n random dots on grid.
n = 4
ID = range(1,n+1)


### Generate random x,y POSition and cooresponding VALue
'''
x_pos = [int(i) for i in np.random.uniform(0, width, n)]
y_pos = [int(i) for i in np.random.uniform(0, height, n)]

val = np.random.uniform(0., 1., n) #np.random.uniform(low=0., high=1., size=n)
y_val = np.random.uniform(0., 1., n)

#for xp,yp,xv,yv in zip(x_pos,y_pos,x_val,y_val):
'''

### Generate random x,y position
xpos = [int(i) for i in np.random.uniform(0, width, n)]
ypos = [int(i) for i in np.random.uniform(0, height, n)]

### Change the above n random positions into n id
for xp,yp,i in zip(xpos,ypos,ID):
  Grid[yp][xp] = i


### Print Grid neatly, with n random dots
print ''
strGrid0 = [' '.join([str(x) for x in Grid[y]]) for y in range(height)]
for h in range(height):
  #print Grid[h]
  print strGrid0[h]
print ''




### Now do Voronoi
for y0 in range(height):
  for x0 in range(width):
    # [0,0], [0,1], ...
    # [1,0], [1,1], ...
    rArray = []
    for i,x,y in zip(ID,xpos,ypos):
      r = sqrt( (x-x0)**2 + (y-y0)**2 )
      rArray.append(r)
    Grid[y0][x0] = rArray.index(min(rArray))+1
    '''
    print ''
    for h in range(height):
      print Grid[h]
    print ''
    #time.sleep(0.5)
    '''

### Print Grid neatly, filled
'''
for h in range(height):
  print Grid[h]
print ''
'''
strGrid1 = [' '.join([str(x) for x in Grid[y]]) for y in range(height)]
for h in range(height):
  print strGrid1[h]
print ''

