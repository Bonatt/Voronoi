import ROOT
import numpy as np
#import math as math
#import os
#import linecache
#import sys
#import time



#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
#ROOT.gStyle.SetPalette(53);
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(1);  #0,1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();




# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()...
def sqrt(x):
  return np.sqrt(x)





### See http://pcg.wikidot.com/pcg-algorithm:voronoi-diagram



### Create empty grid (array)
size = 300 # 30 shows Identifiers array, salted array, and filled array neatly. 68 too high, perfect width. 62 shows full filled only.
height = size
screenratio = 1366./720.
width = int(round(size*screenratio)) #size


### Let salted values be identified by integers (instead of colors, eg)
# N random values
N = 100
MaxUniqueN = N




def GenerateIdentifiers(n):
  # Static incremental list. All values unique.
  #ids = [i for i in range(1,n+1)]
  # Repeat pattern to make more than one population of 1's, e.g., a la camo. Only max 9 unique values allowed. Identifiers = 1-9
  #ids = [i%9+1 for i in range(n)]
  # Same as above, but randomized to for more fair number of 1's, e.g.
  ids = [int(i) for i in np.random.uniform(1, MaxUniqueN+1, n)]
  return ids

Identifiers = GenerateIdentifiers(N)

print ''
print Identifiers
print ''




# If any Identifiers would be greater than one digit, make all emptychars two+ digits to align better.
emptychar = str('').zfill(len(str(max(Identifiers))))
# From https://docs.python.org/2/faq/programming.html#how-do-i-create-a-multidimensional-list . This is best/recommended?
#Map0 = [[emptychar] * width for i in range(height)]
Map = [[emptychar] * width for i in range(height)] #Map0[:]




### Define function so salt N points into matrix
def SaltFunction(d,n):
  decayconstant = 0.1
  sf = [int(i) for i in np.random.uniform(0,d,n)]
  #sf = [int(round(d*(1-np.exp(-i*decayconstant)))-1) for i in np.random.uniform(0,d,n)] #range(1,n+1)]
  #sf = [int(round(d*(1-np.exp(-i*decayconstant)))-1) for i in range(1,n+1)]

  #xcenter = int(round(width/2))
  #ycenter = int(round(height/2))
  #amp = 1.
  #center = round(d/2)
  #sigma = 1.
  #sf = [amp * 1/(sigma*sqrt(2.*np.pi)) * np.exp(-0.5*((i-center)/sigma)**2) for i in np.random.uniform(0,d,n)]
  #print sf
  return sf














### Generate random x,y position. Does not check of two points overlap.
xpos = SaltFunction(width, N) #[int(i) for i in np.random.uniform(0, width, N)]
ypos = SaltFunction(height, N) #[int(i) for i in np.random.uniform(0, height, N)]


### Change the above n random positions into n Identifiers
for xp,yp,i in zip(xpos,ypos,Identifiers):
  Map[yp][xp] = i




### Print Map neatly, with N random dots
strMap0 = [' '.join([str(x).zfill(len(str(max(Identifiers)))) for x in Map[y]]) for y in range(height)]

with open('Voronoi0.txt', 'w') as file:

  for h in range(height):
    #print strMap0[h]

    file.write(strMap0[h]+'\n')

print ''




### Now do Voronoi
for y0 in range(height):
  for x0 in range(width):
    # [0,0], [0,1], ...
    # [1,0], [1,1], ...

    rArray = []
    for x,y in zip(xpos,ypos):
      r = sqrt( (x-x0)**2 + (y-y0)**2 )
      rArray.append(r)

    Map[y0][x0] = Identifiers[rArray.index(min(rArray))] # Works for any Identifiers list




### Print Map neatly, filled. And save to file.
strMap1 = [' '.join([str(x).zfill(len(str(max(Identifiers)))) for x in Map[y]]) for y in range(height)]

with open('Voronoi.txt', 'w') as file:

  for h in range(height):
    #print strMap1[h]
    
    file.write(strMap1[h]+'\n')

print ''
