import ROOT
import numpy as np
#import math as math
#import os
#import linecache
#import sys
#import time
from VoronoiMake2 import *




#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(55); # 51:blue. 52:lo=k/hi=w. 53:lo=r/hi=y, 56 rev. 54:lo=b/hi=y. 55:lo=b/hi=r, pale rainbow. none:rainbow 
# Palettes, see https://root.cern.ch/doc/master/classTColor.html#C05. Or for ROOT 5.34: https://people.nscl.msu.edu/~noji/colormap
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(1);  #0,1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();




# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()...
def sqrt(x):
  return np.sqrt(x)





### See http://pcg.wikidot.com/pcg-algorithm:voronoi-diagram

### May get data via "VoronoiData = np.genfromtxt('Voronoi.txt')", or just use what was created.
VoronoiData = np.genfromtxt('Voronoi.txt') # Grid[0]
VoronoiData0 = np.genfromtxt('Voronoi0.txt') #Grid0[:]


### Note that the printed matrix and the plotted matrix will differ by horz mirror. (0,0) is topleft for print, bottomleft for plots.
### Create canvas with no margins or titles
c = ROOT.TCanvas('c', 'c', 720*width/height, 720)
c.SetTopMargin(0)
c.SetBottomMargin(0)
c.SetLeftMargin(0)
c.SetRightMargin(0)

### Initialize 2D hist to be filled with generated map
h2D = ROOT.TH2D('h2D', 'h2D', width,0,width, height,0,height)
h2D.SetStats(0)
h2D.SetTitle(';;')

### Initialize 2D hist to be filled with seed origins
h2D0 = ROOT.TH2D('h2D0', 'h2D0', width,0,width, height,0,height)
h2D0.SetStats(0)


### Create palette. One color per number.
# From https://root.cern.ch/doc/master/classTColor.html#C05
# From https://root-forum.cern.ch/t/th2-drawn-with-colz-option-colors-and-range/14225/8
#colors = Identifiers[:]
#ROOT.gStyle.SetPalette(len(colors), colors)
#levels = Identifiers[:]
levels = [i+1 for i in range(max(Identifiers))]
h2D.SetContour(len(levels), np.double(levels))



### Go from y lines of x z values and put them into proper x,y,z to be graphed.
# Save x,y,z in case we need them.
x = []
y = []
z = []
for w in range(width):
  for h in range(height):
    x.append(w)
    y.append(h)
    z.append(int(VoronoiData[h][w]))

    h2D.Fill(w,h,VoronoiData[h][w])
    h2D0.Fill(w,h,VoronoiData0[h][w]) #*1000

h2D.Draw('a col')
h2D0.Draw('text same')




### Center of created population? 
# I am confident that the below can be done in a single loop.
# Unique values of z = unique identifiers
UniqueIdentifiers = list(set(z))

# Fill array with index of each identifier
Index = []
for ids in UniqueIdentifiers:
  index = [i for i, zz in enumerate(z) if zz == ids]
  Index.append(index)

# Fill arrays with each coord at index
x2 = []
y2 = []
for index in Index:
  #for i in range(index):
  x2.append([x[i] for i in index])
  y2.append([y[i] for i in index])
 
# Average x,y for each identifer
x3 = []
y3 = []
for i in range(len(UniqueIdentifiers)):
  x3.append(int(round(np.mean(x2[i]))))
  y3.append(int(round(np.mean(y2[i]))))

### Initialize 2D hist to be filled with population centers
h2Dc = ROOT.TH2D('h2Dc', 'h2Dc', width,0,width, height,0,height)
h2Dc.SetStats(0)

for xx,yy,ids in zip(x3,y3,UniqueIdentifiers):
  h2Dc.Fill(xx,yy,ids)

h2Dc.Draw('text45 same')




### Area of created population?
Areas = [len(Index[i]) for i in range(len(Index))]
AreasPercent = [i/float(width*height)*100 for i in Areas]

for i in range(len(UniqueIdentifiers)):
  print 'Identifier:',UniqueIdentifiers[i], '\t Area:', str(Areas[i])+', '+str(int(round(AreasPercent[i])))+'%'



c.SaveAs('hVoronoi.png')








### How to draw own lines between regions?
### How to antialias?
